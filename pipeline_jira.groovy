def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa


pipeline {
    agent any;


    stages {
        stage("Step one") {
            steps {
                script {                                   
                    println "one"
                }
            }
        }
        stage("Step two") {
            steps {
                script {                                    
                    def jira = pa.settings.Jira.new();
                    println(jira.issues.isExist())
                }
            }
        }              
    }
}
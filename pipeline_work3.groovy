
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def Maven, InClass, MyClass
def Func

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
                    Func = lib.pa.settings.Func.new()
                    MyClass = lib.pa.settings.MyClass.new(this)
                    echo Maven.hello()
                }
            }
        }
        stage("Step basic2") {
            steps {
                script {
                    InClass = lib.pa.settings.InClass.new(this)
                    echo Maven.hello()
                }
            }
        }
        stage("Step basic3") {
            steps {
                script {
                    Maven = lib.pa.settings.Maven.new(this)
                    echo Maven.hello()
                }
            }
        }
    }
}
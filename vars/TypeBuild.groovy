public enum TypeBuild {
    DEV("DEV"), TEST("TEST"), PROD("PROD");

    TypeBuild(String name_build) {
        this.name_build = name_build;
    }
            
    private String name_build;
}
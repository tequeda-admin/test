class Log implements Serializable {
    String ANSI_RED = "\033[31m";
    String ANSI_RESET = "\033[0m";
    String ANSI_GREEN = "\033[32m";
    String ANSI_BLUE = "\033[34m";
    String ANSI_BOLD = "\033[1m";

    Object script;

    def init(script) {
        this.script = script;
        if(!script.env?.DEBUG) {
            script.env.DEBUG = "false";
        }
    } 

    def message(message) {     
        script.ansiColor('css') {
            script.println("=" * 80);
            script.println(ANSI_BOLD + message + ANSI_RESET);
            script.println("=" * 80);
        }
    }

    def error(message) {
        script.ansiColor('css') {
            script.println(ANSI_RED + message + ANSI_RESET);
        }
    }

    def info(message) {    
        script.println(message);
    }

    def success(message) {    
        script.ansiColor('css') {
            script.println(ANSI_GREEN + message + ANSI_RESET);
        }
    }

    def debug(message) {
        if(script.env.DEBUG == "true" || 
           (script.params?.DEBUG && (script.params.DEBUG == "true"))
        ) {
            script.ansiColor('css') {
                script.println(ANSI_BLUE + message + ANSI_RESET);
            }
        }
    }
}

/*public class ENUMS implements Serializable {
    public String hello = "HELLO ENUM";
    //def TypeBuild = Enum_TypeBuild;

    public enum TypeBuild {
        DEV("DEV"), TEST("TEST"), PROD("PROD");

        TypeBuild(String name_build) {
            this.name_build = name_build;
        }
            
        private String name_build;
    }

}
*/

class ENUMS implements Serializable{
    public String hello = "HELLO ENUM";
    def t_build;

    ENUMS() {
        t_build = TypeBuild;
    }

    public enum TypeBuild {
        DEV("DEV"), 
        TEST("TEST"), 
        PROD("PROD");

        TypeBuild(String name_build) {
            this.name_build = name_build;
        }

        public static getValues() {
            def list = [];
            for(TypeBuild t: values()) {
                list.add(t);
            }
            return list;
        }
                
        private String name_build;
    }
}
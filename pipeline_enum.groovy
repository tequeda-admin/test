//@Library('sh-lib') _

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

public enum NotificationType {    
    ALL  ("ALL"),
    MAIL ("MAIL");

    public String note;

    NotificationType(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }
}

pipeline {
    agent any;

    stages {
        stage("Step env in stage") {
            steps {
                script {
                    stage_enum(this, NotificationType.MAIL.getNote())
                }
            }
        }
    }
}
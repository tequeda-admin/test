def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa


pipeline {
    agent any;

    stages {
        stage("Step init ") {
            steps {
                script {
                    echo getCurrentDateTime("dd-MM-yyyy_HH-mm-ss")
                }
            }
        }
        stage("Step init 1") {
            steps {
                script {
                    ClassForDate = pa.settings.ClassForDate.new(this)
                    echo ClassForDate.getDate()
                }
            }
        }
    } 
}
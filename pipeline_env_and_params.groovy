
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)



pipeline {
    agent any;

    environment {
        BRANCH_NAME = "hello world"
    }

    parameters {
        string(name: "PROJECT_NAME", defaultValue: "loadgen", description: "NAME")
    }

    stages {
        stage("Step basic") {
            steps {
                script {

                    echo params.PROJECT_NAME
                    echo env.BRANCH_NAME
                        
                    echo "0"
                }
            }
        }
        stage("Step basic1") {
            steps {
                script {
                    env.BRANCH_NAME = "HAI WORLD"

                    echo params.PROJECT_NAME
                    echo env.BRANCH_NAME
                        
                    echo "1"
                }
            }
        }
        stage("Step basic2") {
            steps {
                script {
                    params.PROJECT_NAME = "genolud"

                    echo params.PROJECT_NAME
                    echo env.BRANCH_NAME
                        
                    echo "2"
                }
            }
        }                
    }
}
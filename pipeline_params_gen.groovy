
def lib = library (
    changelog: false, 
    identifier: 'test@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/test.git'
    ])
)

def pa = lib.pa

properties([
    parameters([
        [$class: 'CascadeChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT', //'PT_SINGLE_SELECT',
            description: 'Select a choice',
            name: 'TYPE_REPORT',
            script: [$class: 'GroovyScript',
                //fallbackScript: [classpath: [], sandbox: false, script: 'return ["nothing"]'],
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                       return ["one", "two"]
                    """
                ]
            ]
        ],
        [$class: 'DynamicReferenceParameter',
            choiceType: 'ET_FORMATTED_HTML', //'PT_SINGLE_SELECT',
            description: 'Select a choice',
            //filterLength: 1,
            //filterable: false,
            referencedParameters: 'TYPE_REPORT',
            name: 'component',
            script: [$class: 'GroovyScript',
                fallbackScript: [classpath: [], sandbox: false, script: 'return "<p>fail</p>"'],
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        if(TYPE_REPORT == "one") {
                            return "<input name='value' class='jenkins-input' type='text' value='hello'>"
                        } else return ""
                    """
                ]
            ]
        ],
        [$class: 'CascadeChoiceParameter', 
            choiceType: 'PT_CHECKBOX', 
            description: 'Select Environment',
            filterLength: 1,
            filterable: false,
            name: 'Environment5',
            referencedParameters: 'TYPE_REPORT', 
            script: [
                $class: 'GroovyScript', 
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        if(TYPE_REPORT.equals("one")) {
                            return "Зажата TEST";
                        } else {
                            return["DEV"];
                        }
                    """
                ]
            ]
        ],
        [$class: 'CascadeChoiceParameter', 
            choiceType: 'PT_CHECKBOX', 
            description: 'Select Environment',
            filterLength: 1,
            filterable: false,
            name: 'Environment6',
            //referencedParameters: 'Environment5', 
            script: [
                $class: 'GroovyScript', 
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        //if(Environment5.equals("DEV")) {
                        //    return "Зажата DEV";
                        //} else {
                            return["TEST"];
                        //}
                    """
                ]
            ]
        ],
        [$class: 'CascadeChoiceParameter', 
            choiceType: 'PT_CHECKBOX', 
            description: 'Select Environment',
            filterLength: 1,
            filterable: false,
            name: 'Environment7', 
            //referencedParameters: 'Environment5,Environment6',
            script: [
                $class: 'GroovyScript', 
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        return["PROD"];
                    """
                ]
            ]
        ],/*
        [$class: 'CascadeChoiceParameter', 
            choiceType: 'PT_CHECKBOX', 
            description: 'Select Environment',
            filterLength: 1,
            filterable: false,
            name: 'Environment2',
            script: [
                $class: 'GroovyScript', 
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """

                            return["DEV", "TEST", "PROD"]

                    """
                ]
            ]
        ],        */
        [$class: 'DynamicReferenceParameter',
            choiceType: 'ET_FORMATTED_HTML', //'PT_SINGLE_SELECT',
            //description: '',
            //filterLength: 1,
            //filterable: false,
            omitValueField: true,
            referencedParameters: 'Environment5',
            name: 'component',
            script: [$class: 'GroovyScript',
                fallbackScript: [classpath: [], sandbox: false, script: 'return "<p>fail</p>"'],
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        if(Environment5.contains("DEV")) {
                            return "<input name='value' class='jenkins-input' type='text' value='hello'>"
                        } else return ""
                    """
                ]
            ]
        ],/*
        [$class: 'DynamicReferenceParameter',
            choiceType: 'ET_FORMATTED_HTML', //'PT_SINGLE_SELECT',
            description: '',
            //filterLength: 1,
            //filterable: false,
            omitValueField: false,
            referencedParameters: 'Environment2',
            name: 'component',
            script: [$class: 'GroovyScript',
                fallbackScript: [classpath: [], sandbox: false, script: 'return "<p>fail</p>"'],
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        if(Environment2.contains("QA")) {
                            return "<input name='value' class='jenkins-input' type='text' value='hello'>"
                        } else return ""
                    """
                ]
            ]
        ],/*
        [$class: 'DynamicReferenceParameter',
            choiceType: 'ET_FORMATTED_HTML', //'PT_SINGLE_SELECT',
            description: '',
            //filterLength: 1,
            //filterable: false,
            referencedParameters: 'Environment',
            name: 'component',
            script: [$class: 'GroovyScript',
                fallbackScript: [classpath: [], sandbox: false, script: 'return "<p>fail</p>"'],
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        if(Environment == "Staging") {
                            return "<input name='value' class='jenkins-input' type='text' value='hello'>"
                        } else return ""
                    """
                ]
            ]
        ],
        [$class: 'DynamicReferenceParameter',
            choiceType: 'ET_FORMATTED_HTML', //'PT_SINGLE_SELECT',
            description: '',
            //filterLength: 1,
            //filterable: false,
            referencedParameters: 'Environment',
            name: 'component',
            script: [$class: 'GroovyScript',
                fallbackScript: [classpath: [], sandbox: false, script: 'return "<p>fail</p>"'],
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        if(Environment == "Production") {
                            return "<input name='value' class='jenkins-input' type='text' value='hello'>"
                        } else return ""
                    """
                ]
            ]
        ],*/
    ])
])

pipeline {
    agent any;

    stages {
        // Обращение к содержимому класса
        stage("Step !!!") {
            steps{
                script {
                    println "Hello"
                }
            }
        }  
    }
}
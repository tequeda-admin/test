/*
 Вызов груви файлов из библиотеки

 функция vars.basic вызывает класс, 
 если в класс передать scripts то логи будут писаться 
 
 todo: как передаются дженкинс классы ?
*/

@Library('sh-lib') _

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                basic()
            }
        }
    }
}
//@Library('sh-lib') _

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa

try {
    def THIRD = "!!!"
} catch(Exception e) {
    println("BAD")
}

pipeline {
    agent any;

    environment {
        FIRST = "HELLO"
        SECOND = "WORLD"
    }

    stages {
        stage("Step env in stage") {
            steps {
                script {
                    stage_env_in_stage(this)
                }
            }
        }
        stage("Step env in stage 2") {
            steps {
                script {
                    stage_env_in_stage1(this)
                }
            }
        }
    }
}
//@Library('sh-lib') _
env.VALUE_CRON = "*/4 * * * *"

pipeline {
    agent any;

    parameters{
        string(name: "cron_time", defaultValue: "*/4 * * * *", description: "")
        string(name: "isJira", defaultValue: "true", description: "false")
        string(name: "nameProject", defaultValue: "loadgen-dev", description: "")
    }

    triggers{
        cron("${env.VALUE_CRON}")
    }   
/*
    triggers{
        parameterizedCron(
            """
            ${env.VALUE_CRON}
            """
        )
    }
*/
    stages{
        stage("First") {
            steps {
                script{
                    println("HELLO!!!")
                    println params.cron_time
                    println params.isJira
                    println params.nameProject
                    println env.NAME_SERVER
                }
            }
        }
    }
}

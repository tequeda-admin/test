def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa

pipeline {
    agent any;
    parameters {
        string(name: "one", defaultValue: "", description: "one")
        string(name: "two", defaultValue: "two", description: "two")
    }

    stages {
        stage("Step basic") {
            steps {
                script {
                    if((!params.one || params.one == "") && (!params.two || params.two == "")) {
                        echo "HELLO"
                    }
                    echo "END"
                }
            }
        }
    }
}
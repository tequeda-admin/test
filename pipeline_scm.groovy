// List of all configured branches
def allBranches = scm.branches

// Only the first configured branch name
def gitBranch = (scm.branches[0].name).toString().replace("*/", "")


def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa


pipeline {
    agent any;

    stages {
        stage("Step configs1") {
            steps{
                script {
                    echo gitBranch

                }
            }
        }        
    }
}
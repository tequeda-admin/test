

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa

def func(al) {
    println("FUNC")
    println(al)

    return false
}

pipeline {
    agent any;

    stages {
        stage("Step configs1") {
            steps{
                script {
                    def n1 = false


                    if(n1 && func(n1)) {
                        println("n1+")
                    } else if(!n1 && func(n1)) {
                        println("n1-")
                    }

                }
            }
        }        
    }
}
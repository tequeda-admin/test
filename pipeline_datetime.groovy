import java.text.SimpleDateFormat;  
import java.time.Date;  


pipeline {
    agent any;

    stages {
        // Обращение к содержимому класса
        stage("Step configs0") {
            steps{
                script {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
                    Date date = new Date(); 
                    def current_time = formatter.format(date);
                    def old_time = formatter.format("10/01/2021 10:30:20");

                    echo old_time.toString()
                }
            }
        }        
    }
}

def a = library (
    changelog: false, 
    identifier: 'test@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/test.git'
    ])
)

def b = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
                    Log.init(this)
                    Log.error("HELLO")

                    //throw new Exception("asd");
                    try {
                        //exc(this)
                        exc1(this)
                        //Unt.func()
                        //throw new Unt("hello")
                        //new PipeError.Unt1("hello unstable");
                    } catch(Exception ex) {
                        println(ex)
                    }
                }
            }
        }
    }
}
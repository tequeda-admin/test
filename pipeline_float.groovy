import java.lang.Math;
import java.math.BigDecimal;

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

String str1;
String str2;

Float num1, num2;
pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
str1 = "Размер файла: 13567351467 bytes"
str2 = "Размер файла: 13567351499 bytes"
                }
            }
        }
        stage("Step basic2") {
            steps {
                script {
def match1 = /-?\d{1,}.?,?\d+/

def matcher1 = str1.split(":")[-1] =~ match1;
println(matcher1[0]);
num1 = matcher1.find() ? Float.parseFloat(matcher1[0]) : 0.00;
println(num1)
def matcher2 = str2.split(":")[-1] =~ match1;
println(matcher2[0]);
num2 = matcher2.find() ? Float.parseFloat(matcher2[0]) : 0.00;
println(num2)



BigDecimal numa1 = matcher1.find() ? (new BigDecimal(matcher1[0])) : 0.00;
BigDecimal numa2 = matcher2.find() ? (new BigDecimal(matcher2[0])) : 0.00;
println(numa1)
println(numa2)
println(numa1 - numa2)
println("orig" + String.format("%.2f", numa1 - numa2))
                }
            }
        }
        stage("Step basic3") {
            steps {
                script {
println(String.format("%.2f", num1 - num2))
                }
            }
        }        
    }
}
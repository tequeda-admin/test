import hudson.model.Result as TypeStatus;

pipeline {
    agent any;

    parameters {
        string(name: "param1", defaultValue: "abc", description: "PARAM1")
        booleanParam(name: "param2", defaultValue: "cxe", description: "PARAM2")
    }

    stages {
        stage("Step basic") {
            steps {
                script {
                    println(params.param1)
                    println(params.param2)

                    env.STST = "My1";

                    //throw new Exception("FAILED");
                    //currentBuild.result = "FAILURE"
                    //return TypeStatus.ABORTED;
                    //env.STAGE_NAME = env.STAGE_NAME;
                }
            }
        }    
        stage("Step basic2") {
            steps {
                script {
                    println(params.param1)
                    println(params.param2)

                    env.STST = "My2";
                    //env.STAGE_NAME = env.STAGE_NAME;
println env.STAGE_NAME
                    //throw new Exception("FAILED");
                    currentBuild.result = "FAILURE"
error "FAILURE"                    
                    //return TypeStatus.ABORTED;
                }
            }
        }
        stage("Step basic3") {
            steps {
                script {
                    println(params.param1)
                    println(params.param2)

                    env.STST = "My3";

                    //throw new Exception("FAILED");
                    currentBuild.result = "SUCCESS"
                    //return TypeStatus.ABORTED;
                    //env.STAGE_NAME = env.STAGE_NAME;
                }
            }
        }
    }
    post{
        always{
            println "EEEEENDDDDD!!!"
        }
    }
}
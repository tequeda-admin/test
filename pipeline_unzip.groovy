
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)



pipeline {
    agent any;


    stages {
        stage("Step skip error") {
            steps {
                script {
                    def res = sh(script: """#!/bin/bash
                    set +x;
                    set +e;
                    gunzip -f hello > world.zip >&2
                    set -e;
                    set -x;
                    """, returnStdout: true)
                    sh "find . -type f | xargs rm"
                
                    println res
                }
            }
        }  
        stage("Step yes file") {
            steps {
                script {
                    try {
                    sh "echo \$PWD"
                    println "1"
                    sh "touch hello"
                    println "2"
                    sh "echo 'message' > hello"
                    println "3"
                    sh "gzip hello"
                    sh "ls -a"
                    sh(script:  "gunzip -f hello > world.zip", returnStdout: false)
                    println "4"
                    sh "ls -a"
                    println "5"
                    sh "find . -type f | xargs rm"
                    } catch(Exception ex){
                        println ex
                    }
                }
            }
        } 
        stage("Step basic") {
            steps {
                script {
                    sh "gunzip -f hello > world.zip"
                }
            }
        }              
    }
}
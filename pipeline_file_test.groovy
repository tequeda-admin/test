
import java.io.Reader
import java.io.InputStream

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

pipeline {
    agent any;

    stages {
        stage("Step zero") {
            steps {
                script {
                    sh("rm ${WORKSPACE}/text.txt")
                    sh("for i in 1 2 3 4 5 6 7 8 ; do echo \"\$i\" >> text.txt; done")
                    sh("cat text.txt")
                }
            }
        }
        stage("Step uno") {
            steps {
                script {
                    def inputFile = new File("${WORKSPACE}/text.txt")

                    inputFile.withReader { Reader reader ->
                        reader.eachLine { String line ->
                            println line

                            if(i > 5) {
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa

def CURRENT_STATUS = "TRUE"
def current_status = "1"

def change() {
    CURRENT_STATUS = "FALSE"
    current_status = "2"
}

pipeline {
    agent any;

    stages {
        stage("Step env1") {
            steps{
                script {
                    change()
                }
            }
        }    
        stage("Step env2") {
            steps{
                script {
                    echo CURRENT_STATUS
                    echo current_status
                }
            }
        }       
    }
}
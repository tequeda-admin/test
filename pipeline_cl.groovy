import java.util.Base64

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://florapos@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)



pipeline {
    agent any;


    stages {
        stage("Step basic") {
            steps {
                script {
                    def Cls = lib.pa.settings.Mail.new()
                    println Cls.sendMsg()
                }
            }
        }              
    }
}
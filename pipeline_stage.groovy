@Library('sh-lib') _

def gitBranch = 'master'

pipeline {
    agent any;
    
    stages{
        stage("First") {
            steps {
                stage0(this)
            }
        }
        stage("Second") {
            steps {
                stage1()
            }
        }
    }
}

def lib = library (
    changelog: false, 
    identifier: 'test@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/test.git'
    ])
)
def pa = lib.pa
init(this)/*
node {
    stage("init_zero") {
        init(this)
        Configs.text = "Real world";
        println Configs.text
        println Configs.creds.h1     
    }
}*/

pipeline {
    agent any;

    parameters{
        choice(name: "test", choices: Configs.lll, description: "TEST:")
    }

    stages {
        // Обращение к содержимому класса
        stage("Step !!!") {
            when {
                expression { Configs.all_right }
            }
            steps{
                script {
                    println env.PARAM1
                    println env.PARAM2

                    Configs.text = "Hello world";
                    println Configs.text

                    def Cron = Configs.getClassCron();
                    Cron.text = "tetete";
                    println Cron.text

                    Configs.addCron(Cron);
                }
            }
        }
        // Изменение содержимого класса
        stage("Step configs1") {
            steps{
                script {
                    touchConfigs(this)

                    Configs.crons.each{ cron->
                        println cron.text
                    }

                    Stage4(this);
                }
            }
        }    
    }
    post {
        always {
            script {
                node {
                    cleared(this);
                }
            }
        }
    }
}
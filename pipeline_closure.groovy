import java.util.Base64

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)



pipeline {
    agent any;


    stages {
        stage("Step basic") {
            steps {
                script {
                    m1 = [0: ["num1", "num2"], 1: ["num1"]]
                    m2 = ["num1": "h1", "num2": "h3"]

                    def getMails = { num, groups ->
                    def mails = []
                    groups.each { group ->
                        mails.add(m2."$group")
                    }
                    return ["$num": "$mails"]
                    }

                    def r1 = [:]
                    m1.each { num, groups ->
                    def res = getMails(num, groups)
                        
                    r1.putAll(res)
                    }

                    print r1

                }
            }
        }              
    }
}
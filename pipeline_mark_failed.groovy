
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)


pipeline {
    agent any;


    stages {
        stage("Step one") {
            steps {
                script {                                   
                    println "one"
                }
            }
        }
        stage("Step two") {
            steps {
                script {                                    
                    println "two"

                    try{
                        error "HELLO"
                    } catch(Exception e) {
                        setStageFailure(this, e)
                    }

                    
                }
            }
        }
        stage("Step three") {
            steps {
                script {                                       
                    println "three"
                }
            }
        }               
    }
}
//@Library('sh-lib') _

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

pipeline {
    agent any;

    stages {
        stage("Step env in stage") {
            steps {
                script {
                    echo "1"
                    def FuncInClass = lib.pa.settings.FuncInClass.new()
                    echo "2"
                    FuncInClass.init()
                    echo "3"
                    def text = FuncInClass.hey()
                    echo text
                }
            }
        }
    }
}
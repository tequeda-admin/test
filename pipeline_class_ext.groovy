
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def OriginClass, ExtClass

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
                    OriginClass = lib.pa.settings.MyClassPar.new(this)
                    OriginClass.run1()
                }
            }
        }
        stage("Step extend") {
            steps {
                script {
                    ExtClass = lib.pa.settings.MyClassExt.new(this)
                    println(ExtClass.textMessage())
                }
            }
        }
    }
}
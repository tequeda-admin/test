
def runJob(jobName, params){
    def job

    try {
        job = build job: jobName, parameters: params
    } catch(err) {
        return false
    }

    return job.getBuildVariables()
}


def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def list_items = ["a", "b", "c"]

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
                    
                }
            }
        /*
        stage("Step basic") {
            steps {
                script {
                    firstJobResult = runJob('work2', [
                        string(name: 'message', value: 'Hello first JOB from main pipeline')
                    ])
                
                    if(firstJobResult == false) {
                        currentBuild.status = 'FAILURE'
                        return
                    }

                    echo "First job return data: ${firstJobResult.returnData}"
                }
            }*/
        }
    }
}

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
                    testError(this);
                }
            }
        }
    }
}
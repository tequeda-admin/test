import hudson.model.Result as TypeStatus;

pipeline {
    agent any;

    parameters {
        string(name: "param1", defaultValue: "", description: "PARAM1")
        booleanParam(name: "param2", defaultValue: "", description: "PARAM2")
    }

    stages {
        stage("Step basic") {
            steps {
                script {
                    println(params.param1)
                    println(params.param2)

                    env.STST = "My";

                    return TypeStatus.SUCCESS;
                }
            }
        }
    }
}
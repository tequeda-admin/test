import hudson.model.Result as TypeStatus;

pipeline {
    agent any;

    parameters {
        string(name: "param1", defaultValue: "", description: "PARAM1")
        booleanParam(name: "param2", defaultValue: "", description: "PARAM2")
        hidden(name: "hid", defaultValue: "", description: "hidden")
    }

    stages {
        stage("Step basic") {
            steps {
                script {
                    println(params.param1)
                    println(params.param2)
                    println("===")
                    println(params.hid)
                    println("===")
                    env.STST = "My";

                    return TypeStatus.SUCCESS;
                }
            }
        }
    }
}
import groovy.json.JsonSlurper


//@Library('sh-lib') _

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)


def forJSON

pipeline {
    agent any;

    stages {
        stage("Step json1") {
            steps {
                script {
                    String txt = '{"first": "second", "lang": "ita"}'
                    def jsonSlurper = new JsonSlurper()
                    def object = jsonSlurper.parseText(txt)
                    echo "${object.first}"

                    Map mm = ["a": "1", "b": "2"]
                    def json_string = groovy.json.JsonOutput.toJson(mm)
                    def json_pretty = groovy.json.JsonOutput.prettyPrint(json_string)
                    echo "${json_pretty}"
                }
            }
        }      /*  
        stage("Step json2") {
            steps {
                script {
                    forJSON = lib.pa.settings.ForJSON.new(this)
                    def result = forJSON.getDataJSon()
                    sh("ls -a")
                    echo "${result.first}"
                }
            }
        }
        stage("Step json3") {
            steps {
                script {
                    //forJSON = lib.pa.settings.ForJSON.new(this)
                    def result = forJSON.getDataJSon()
                    sh("ls -a")
                    echo "${result.first}"
                }
            }
        }*/
    }
}
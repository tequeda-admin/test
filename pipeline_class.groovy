
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)
def pa = lib.pa

def MyClassDef

def arr_one = [:]

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
                    arr_one.put("one", "two")
                    arr_one.put("one", "two")
                    arr_one.put("one", "two")

                    MyClassDef = pa.settings.MyClassDef.new()
                        
                    echo "1"
                }
            }
        }
        stage("Step basic2") {
            steps {
                script {
                    usedClass(this, MyClassDef)

                    st0(this)

                    echo "2"
                }
            }
        }
        stage("Step basic3") {
            steps {
                script {
                    echo MyClassDef.par1
                    echo MyClassDef.run0()

                    echo "3"
                }
            }
        }
    }
}
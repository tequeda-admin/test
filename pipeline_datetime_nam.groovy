import java.text.SimpleDateFormat;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.MONTH; 
import static java.util.Calendar.DATE;   
import java.time.LocalDate;

pipeline {
    agent any;

    stages {
        // Обращение к содержимому класса
        stage("Step configs0") {
            steps{
                script {
                  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
                  Date date = new Date(); 
                  def current_time = formatter.format(date);    

                  SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");                
                  def old_time = formatter2.parse("28/12/2022 10:30:20")

                  println current_time
                  println old_time

                    println date[YEAR]
                    println (date[MONTH] -1)

                  def years = date[YEAR] - old_time[YEAR] 
                  def months = date[MONTH] - old_time[MONTH]

                  if(months < 0){
                    years = years - 1
                    months = months + 12;
                  }

                  def days = date[DATE] - old_time[DATE]
                  if(days < 1) {
                    months = months - 1
                      
                    if(months < 1){
                      years = years - 1
                      months = months + 12;
                    }
                    
                      /* !!! ATTENTION
                      Месяца возвращаются со счетом от нуля, количество дней месяц возвращается для 1+
                      Реально должно быть (index_last_month -1 +1) т.к. нам нужен прошлый месяц
                      */
                    def index_last_month = date[MONTH];
                    if(index_last_month < 10){
                      index_last_month = "0$index_last_month"
                    }

                    LocalDate m_date = LocalDate.parse("${date[YEAR]}-${index_last_month}-01");
                    def days_month = m_date.lengthOfMonth();

                      days = days + days_month;
                  }

                  println "y:$years m:$months d:$days"


                }
            }
        }        
    }
}

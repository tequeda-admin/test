//@Library('sh-lib') _

def my_param = "123"


    node {
        stage("init_zero") {

                my_param = "===="
                echo "HEEEEEEEEE"
            
        }
    }


pipeline {
    agent any;
    
    stages{
        stage("First") {
            steps {
                script {
                    echo my_param
                    env.NAME_SERVER = "MY_SERVER"
                    
                    def p1 = [
                        string(name: "cron_time", value: "*/1 * * * *"),
                        string(name: "isJira", value: "true"),
                        string(name: "nameProject", value: "loadgen")
                    ] 

                    build(job: "cron_zero", 
                        parameters: p1,
                        propagate: true,
                        wait: false)  

                    def p2 = [
                        string(name: "cron_time", value: "*/2 * * * *"),
                        string(name: "isJira", value: "false"),
                        string(name: "nameProject", value: "loadgen")
                    ] 

                    build(job: "cron_zero", 
                        parameters: p2,
                        propagate: true,
                        wait: false)  
                }
            }
        }
    }
}

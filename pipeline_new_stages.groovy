
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

pipeline {
    agent none;
    stages {
        stage('Init') {
            agent any;

            steps {
                script {
                    def Phases = lib.pa.settings.Phases.new(this);

                    stage("one") {
                        script {
                            Phases.stage_1()
                        }
                    }

                    stage("two") {
                        script {
                            Phases.stage_2()
                        }
                    }
                }
            }
        }
        stage('DUE') {
            steps{
                script{
                    def b = stages_in_var()
                }
            }
        }
    }
}
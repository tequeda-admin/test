@Library('sh-lib') _

configs = new pa.settings.Configs()


pipeline {
    agent any;
    
    stages{
        stage("First") {
            steps {
                import0(configs)
            }
        }
        stage("Second") {
            steps {
                import1(configs)
            }
        }
        stage("Third") {
            steps {
                import2()
            }
        }
    }
}
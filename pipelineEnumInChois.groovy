
library (
    changelog: false, 
    identifier: 'test@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/test.git'
    ])
)

init(this)
println(ENUMS.t_build.DEV.toString())
println(ENUMS.t_build.getValues())
env.L_BUILD = ENUMS.t_build.getValues().collect{ "'$it'" }.join(',');
println "${ENUMS.t_build.getValues()[0]} != DEV"
println "==="
println ENUMS.t_build.getValues().collect{ "${it}HELLO" }
println ENUMS.t_build.getValues().collect{ it.toString() == "DEV" ?: false }
println ENUMS.t_build.getValues().collect{ it.toString() in ["DEV"] ? "${it}:selected": it }
println ENUMS.t_build.getValues().collect{ it.toString() in ["DEV"] ? "${it}:selected": it }.join(',')
println "==="
properties([
    parameters([
        [$class: 'WHideParameterDefinition', name: 'HIDDEN_PARAMS', defaultValue: ENUMS.t_build.getValues().collect{ it.toString() in ["DEV"] ? "${it}:selected": it }.join(','), description: ''],
        [$class: 'CascadeChoiceParameter', 
            choiceType: 'PT_CHECKBOX', 
            description: 'Select Environment',
            filterLength: 1,
            filterable: false,
            name: 'PORT1', 
            referencedParameters: 'HIDDEN_PARAMS',
            script: [
                $class: 'GroovyScript', 
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        return ["MOD","RET"]
                    """
                ]
            ]
        ],
        [$class: 'ChoiceParameter', 
            choiceType: 'PT_RADIO', 
            description: 'Select Environment',
            filterLength: 1,
            filterable: false,
            name: 'Environment', 
            referencedParameters: 'HIDDEN_PARAMS',
            script: [
                $class: 'GroovyScript', 
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        return ["DEV","TEST"]
                    """
                ]
            ]
        ],
        [$class: 'DynamicReferenceParameter',
            choiceType: 'ET_FORMATTED_HTML', 
            omitValueField: true,
            referencedParameters: 'Environment,PORT1',
            name: 'component',
            script: [$class: 'GroovyScript',
                fallbackScript: [classpath: [], sandbox: false, script: 'return "<p>fail</p>"'],
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        if(PORT1.contains("DEV") && Environment.contains("MOD")) {
                            return "<input name='value' class='jenkins-input' type='text' value='hello'>"
                        } else return ""
                    """
                ]
            ]
        ],
        
        /*
        [$class: 'WHideParameterDefinition', name: 'HIDDEN_PARAMS', defaultValue: ENUMS.t_build.getValues().collect{ it.toString() in ["DEV"] ? "${it}:selected": it }.join(','), description: ''],
        [$class: 'CascadeChoiceParameter', 
            choiceType: 'PT_CHECKBOX', 
            description: 'Select Environment',
            filterLength: 1,
            filterable: false,
            name: 'Environment', 
            referencedParameters: 'HIDDEN_PARAMS',
            script: [
                $class: 'GroovyScript', 
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        List lst = HIDDEN_PARAMS.split(',')
                        return lst
                    """
                ]
            ]
        ],
        [$class: 'DynamicReferenceParameter',
            choiceType: 'ET_FORMATTED_HTML', 
            omitValueField: true,
            referencedParameters: 'Environment',
            name: 'component',
            script: [$class: 'GroovyScript',
                fallbackScript: [classpath: [], sandbox: false, script: 'return "<p>fail</p>"'],
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        if(Environment.contains("dev")) {
                            return "<input name='value' class='jenkins-input' type='text' value='hello'>"
                        } else return ""
                    """
                ]
            ]
        ],*/
        /*
        [$class: 'CascadeChoiceParameter', 
            choiceType: 'PT_CHECKBOX', 
            description: 'Select Environment',
            filterLength: 1,
            filterable: false,
            name: 'Environment', 
            //referencedParameters: 'Environment5,Environment6',
            script: [
                $class: 'GroovyScript', 
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                    """
                ]
            ]
        ],
        [$class: 'DynamicReferenceParameter',
            choiceType: 'ET_FORMATTED_HTML', 
            omitValueField: true,
            referencedParameters: 'Environment',
            name: 'component',
            script: [$class: 'GroovyScript',
                fallbackScript: [classpath: [], sandbox: false, script: 'return "<p>fail</p>"'],
                script: [
                    classpath: [], 
                    sandbox: true, 
                    script: """
                        if(Environment.contains(ENUMS.t_build.DEV)) {
                            return "<input name='value' class='jenkins-input' type='text' value='hello'>"
                        } else return ""
                    """
                ]
            ]
        ],*/
    ])
])


pipeline {
    agent any;

    stages {
        // Обращение к содержимому класса
        stage("Step 1") {
            steps{
                script{
                    println params.Environment
                    Stage_type(this);
                }
            }
        }    
    }
}
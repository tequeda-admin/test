def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa

pipeline {
    agent any;

    stages {
        stage("Step configs1") {
            steps{
                script {
                    echo gzip_base64("text")
                }
            }
        }        
    }
}
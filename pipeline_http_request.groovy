
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
                        
                    def response = httpRequest url: 'https://sam-barmen.ru/type/aperitif', httpMode: 'GET', customHeaders:[[name:'Authorization', value:"Basic ${auth}"]]

                    println(response.status)
                    println(response.content)
                }
            }
        }
    }
}
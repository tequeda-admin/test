
def lib = library (
    changelog: false, 
    identifier: 'test@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/test.git'
    ])
)
def pa = lib.pa
init(this)

pipeline {
    agent any;

    parameters{
        choice(name: "test", choices: Configs.lll, description: "TEST:")
    }

    stages {
        // Обращение к содержимому класса
        stage("Step 1") {
            when {
                expression { Configs.all_right }
            }
            steps{
                //script{
                    Stage_type(this);
                //}
            }
        }  
        stage("Step 2") {
            when {
                expression { Configs.all_right }
            }
            steps{
                script {
                    def ll1;
                    println "enum"
                    ll1 = ENUMS.t_build.DEV;

                    println ll1
                }
            }
        }
        stage("Step 3") {
            when {
                expression { Configs.all_right }
            }
            steps{
                //script{
                    Stage_type(this);
                //}
            }
        }    
    }/*
    post {
        always {
            script {
                node {
                    cleared(this);
                }
            }
        }
    }*/
}
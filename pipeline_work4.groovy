
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def Maven, InClass, MyClass
def Func

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
                        
                    echo "1"
                }
            }
        }
        stage("Step basic2") {
            steps {
                script {
                    try {
                        echo "2"
                    
                        error "MESSAGE MESSAGE"
                        echo "0"
                    } catch(Exception e) {
                        echo e
                    } finally {
                        echo "FINISH"
                    }
                }
            }
        }
        stage("Step basic3") {
            steps {
                script {
                    error "MESSAGE MESSAGE"
                    echo "3"
                }
            }
        }
    }
}
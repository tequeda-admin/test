def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa

//def a, b, c

pipeline {
    agent any;

    stages {
        stage("Step 1") {
            steps {
                script {
                    (a, b, c) = xyi(this)
                }
            }
        }
        stage("Step 2") {
            steps {
                script {
                    echo("${a} ${b} ${c}")
                }
            }
        }
    }
}
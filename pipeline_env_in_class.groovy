//@Library('sh-lib') _

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa
def EnvInClass

pipeline {
    agent any;

    stages {
        stage("Step env in class") {
            steps {
                script {
                    try {
                        EnvInClass = testLastLog(this, EnvInClass)
                    } catch(Exception ex) {
                        echo EnvInClass.last_log
                        println("ERROR: " + ex)
                        
                    }
                }
            }
        }

        stage("Step env in class2") {
            steps {
                script {
                    echo "Stage2"
                }
            }
        }
    }
}
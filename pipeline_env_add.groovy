def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa


pipeline {
    agent any;

    environment {
        BRANCH_NAME = "hello world"
    }    

    stages {
        stage("Step env1") {
            steps{
                script {
                    env.CURRENT_STATUS = "fas"
                }
            }
        }    
        stage("Step env2") {
            steps{
                script {
                    echo env.CURRENT_STATUS
                }
            }
        } 
        stage("Step env3") {
            steps {
                script {
                    env.BRANCH_NAME = "HAI WORLD"
                    echo env.BRANCH_NAME                        
                }
            }
        }      
        stage("Step env4") {
            steps {
                script {
                    echo env.BRANCH_NAME                        
                }
            }
        }           
    }
}
import java.util.Base64

initLib()


pipeline {
    agent any;


    stages {
        stage("Step basic") {
            steps {
                script {
                  hello_call_params(this);
                }
            }
        }              
    }
}


def initLib() {
  def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

}

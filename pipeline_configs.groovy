@Library('sh-lib') _

configs = new pa.settings.Configs()

pipeline {
    agent any;

    stages {
        // Обращение к содержимому класса
        stage("Step configs0") {
            steps{
                script {
                    sh "echo ${configs.git.url}"
                }
            }
        }
        // Изменение содержимого класса
        stage("Step configs1") {
            steps{
                script {
                    configs.git.url = "Hello world"
                    sh "echo ${configs.git.url}"
                }
            }
        }
        // Использование функций в классе
        stage("Step configs2") {
            steps{
                script {
                    configs.initGit()
                    sh "echo ${configs.git.url}"
                }
            }
        }
        // Вызов содержимого в функциях
        stage("Step configs3") {
            steps{
                script {
                    use_configs(configs)
                }
            }
        }        
    }
}
//@Library('sh-lib') _

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa

env.FIRST = "Hello"

pipeline {
    agent any;

    stages {
        stage("Step env in stage") {
            steps {
                script {
                    env.SECOND = "Hello2"
                }
            }
        }
        stage("Step env in stage 2") {
            steps {
                script {
                    echo env.FIRST
                    echo env.SECOND
                }
            }
        }
        stage("Step env in stage 3") {
            steps {
                script {
                    printEnv(this)
                    env.SECOND = "HI"
                }
            }
        }
        stage("Step env in stage 4") {
            steps {
                script {
                    echo env.THIRD
                    env.SECOND = "HI"
                }
            }
        }
        stage("Step env in stage 5") {
            steps {
                script {
                    echo env.FIRST
                    echo env.SECOND
                    echo env.THIRD
                }
            }
        }
    }
}
//@Library('sh-lib') _

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa

pipeline {
    agent any;

    stages {
        stage("Step env in stage") {
            steps {
                script {
                    echo env.BUILD_ID
                    echo env.BUILD_URL
                }
            }
        }
    }
}
//@Library('sh-lib') _

def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa

def lib2 = library (
    changelog: false, 
    identifier: 'sh-lib2@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib2.git'
    ])
)

//def f_lib2 = lib2.pa.functions.Func.new();

//configs = pa.settings.Configs.new()
//configs.git.url = "Hai"

pipeline {
    agent any;

    stages {
         // Изменение содержимого класса
        /*stage("Step configs1") {
            steps{
                script {                    
                    def res_func = f_lib2.get_link();
                    echo(res_func);
                }
            }
        }*/
        stage("Step stage in lib") {
            steps {
                script {
                    stage_lib_in_lib(this, lib2)
                }
            }
        }
    }
}
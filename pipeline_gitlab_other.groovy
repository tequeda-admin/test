
def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def runJob(jobName, params){
    def job

    try {
        job = build job: jobName, parameters: params
    } catch(err) {
        return false
    }

    return job.getBuildVariables()
}

pipeline {
    agent any;

    stages {
        stage("Step basic") {
            steps {
                script {
                    firstJobResult = runJob('gitlab_env_in_other', [
                        string(name: 'message', value: 'Hello first JOB from main pipeline')
                    ])
                
                    if(firstJobResult == false) {
                        currentBuild.status = 'FAILURE'
                        return
                    }

                    echo "First job return data: ${firstJobResult.returnData}"
                }
            }
        }
    }
}
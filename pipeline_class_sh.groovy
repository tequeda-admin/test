def lib = library (
    changelog: false, 
    identifier: 'sh-lib@master', 
    retriever: modernSCM([
        $class: 'GitSCMSource',
        remote: 'https://tequeda-admin@bitbucket.org/tequeda-admin/sh-lib.git'
    ])
)

def pa = lib.pa


def MyClassSh
def MyFunc

pipeline {
    agent any;

    stages {
        stage("Step configs1") {
            steps{
                script {
                    echo "1"
                    MyFunc = pa.settings.Func.new()
                    MyFunc.sh_hello()
                }
            }
        }   
        stage("Step configs2") {
            steps{
                script {
                    echo "3"
                    MyClassSh = pa.settings.MyClassSh.new()
                    MyClassSh.init(this)
                    MyClassSh.run()
                    echo "4"
                    MyClassSh.run1()
                }
            }
        }  
        stage("Step configs3") {
            steps{
                script {
                    echo "5"
                    //MyClassSh.init(this)
                    MyClassSh.run()
                }
            }
        }               
        stage("Step configs4") {
            steps{
                script {
                    echo "5"
                    my_f1()
                }
            }
        }        
    }
}


def my_f1() {
    echo "f1"
    MyClassSh.init(this)
    MyClassSh.run1()
}